# HeartLib Game Boy Advance Development API
Comprehensive API for Nintendo GBA inspired by HAMLib and its respective extension, HEL lib.

# The library
This library is undergoing heavy development. It is currenty my most heavily worked on GBA Project.

The library is designed to make GBA development way easier.

HeartLib has a TON of nice little functions for the user to play with that can manipulate registers, sprites, DMA, bitmaps,
palettes, bios calls, SRAM, memory, Interrupts, and a bunch of many other things.

HeartLib comes packed with a bunch of tools that you can use for graphics and sound, and it also comes with a semi-automated build system.

At the moment, it its only compatible with C. It will not work with C++.

# Features
 - 200+ functions for you to use
 
 - MaxMod, the best GBA sound engine built in.
 
 - GBFS
 
 - SRAM
 
 - Function helpers for those commands with a lot of numbers that are hard to interpret.
 
 - PCX image decoding
 
 - JPEG image decoding.
 
 - ALL the GBA hardware registers ready to go, including some undocumented ones
 
 - Functions for ALL the GBA BIOS calls
 
 - Real Time Clock functions.
 
 - Random Number Generator
 
 - Functions for configuring ALL the necessary Registers.
 
 - aPlib compression
 
 - Sprites
 
 - Bitmaps
 
 - Drawing text to the screen in bitmap and tile modes.
 
 - Drawing new data to the screen when scrolling very large backgrounds.
 
 - Tools for graphics conversion, graphics editing, and map making.
 
 - Many more things!
