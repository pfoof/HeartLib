#include "libheart.h"

const char hrt_lang_crash_msg[]="HeartLib has crashed!";
const char hrt_lang_crash_func[]="Function: ";
const char hrt_lang_crash_arg[]="Arg: ";
const char hrt_lang_crash_reset[]="Please reset the console.";

const char hrt_lang_crash_err_invarg[]="Invalid argument.";